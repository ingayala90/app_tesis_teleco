import 'package:app_iris/components/services/firestore_service.dart';
import 'package:app_iris/resource/usuario.dart';
//import 'package:app_iris/services/firestore_service.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class UsuarioProvider with ChangeNotifier {
  final firestoreService = FirestoreService();
  String _nombre;
  double _cedula;
  String _usuarioId;
  String _primapellido;
  String _segapellido;
  String _direccion;
  double _cel;
  String _rh;

  var uuid = Uuid();

  //Getters
  String get nombre => _nombre;
  double get cedula => _cedula;
  String get primapellido => _primapellido;
  String get segapellido => _segapellido;
  String get direccion => _direccion;
  double get cel => _cel;
  String get rh => _rh;

  //Setters
  changeNombre(String value) {
    _nombre = value;
    notifyListeners();
  }

  changeCedula(String value) {
    _cedula = double.parse(value);
    notifyListeners();
  }

  changeprimapellido(String value) {
    _primapellido = value;
    notifyListeners();
  }

  changesegapellido(String value) {
    _segapellido = value;
    notifyListeners();
  }

  changedireccion(String value) {
    _direccion = value;
    notifyListeners();
  }

  changecel(String value) {
    _cel = double.parse(value);
    notifyListeners();
  }

  changerh(String value) {
    _rh = value;
    notifyListeners();
  }

  saveUsuario() {
    var newUsuario = Usuario(
        nombre: nombre,
        cedula: cedula,
        primapellido: primapellido,
        segapellido: segapellido,
        direccion: direccion,
        cel: cel,
        rh: rh,
        usuarioID: uuid.v4());
    firestoreService.saveUsuario(newUsuario);
  }
}
