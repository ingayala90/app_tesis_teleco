import 'package:flutter/material.dart';
import 'package:app_iris/resource/colors.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;
  const RoundedButton({
    Key key,
    this.text,
    this.press,
    this.color = kPrimaryColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.5,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: secondaryColor, // background
            onPrimary: kPrimaryLightColor, // foreground
          ),
          onPressed: press,
          child: Text(
            text,
            style: TextStyle(color: kPrimaryLightColor, fontSize: 24),
          ),
        ),
      ),
    );
  }
}
