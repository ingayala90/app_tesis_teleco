import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Camara extends StatefulWidget {
  @override
  _CamaraState createState() => _CamaraState();
}

class _CamaraState extends State<Camara> {
  //This will explode on Web
  WebViewController _controller;

  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: WebView(
            initialUrl:
                "https://flutterappworld.com/a-web-safe-implementation-of-dart-io-platforms/",
            javascriptMode: JavascriptMode.unrestricted,
            onWebViewCreated: (WebViewController webviewcontroller) {
              _controller = webviewcontroller;
            }),
      ),
    );
  }
}
