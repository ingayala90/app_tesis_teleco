import 'package:app_iris/resource/colors.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;

class Voz extends StatefulWidget {
  @override
  _VozState createState() => _VozState();
}

class _VozState extends State<Voz> {
  stt.SpeechToText _speech;
  bool _islistening = false;
  String _text = '...';

  void _onlisten() async {
    if (!_islistening) {
      bool available = await _speech.initialize(
        onStatus: (val) => print('onStatus: $val'),
        onError: (val) => print('onError: $val'),
      );
      if (available) {
        setState(() {
          _islistening = true;
        });
        _speech.listen(
            onResult: (val) => setState(() {
                  _text = val.recognizedWords;
                }));
      }
    } else {
      setState(() {
        _islistening = false;
        _speech.stop();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _speech = stt.SpeechToText();
  }

  @override
  Widget build(BuildContext context) {
    print("hey!!");
    print(_islistening);
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/Fondo2.jpg"),
                fit: BoxFit.fill)),
        child: Padding(
          padding: EdgeInsets.all(70.0),
          child: Column(children: <Widget>[
            SizedBox(
              height: 10,
            ),
            Text(
              'Tu orden es:',
              style: TextStyle(fontSize: 30),
            ),
            SizedBox(
              height: 10,
            ),
            SizedBox(
                height: 120,
                width: 400,
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(color: Colors.blue, width: 5)),
                  child: Text(
                    _text,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 25.0,
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                )),
            SizedBox(
              height: 40,
            ),
            if (_islistening == false) ...[
              Image.asset(
                'assets/images/1.png',
                height: 350,
              ),
            ],
            if (_islistening == true) ...[
              Image.asset(
                'assets/images/2.png',
                height: 350,
              ),
            ],
            SizedBox(
              height: 40,
            ),
            SizedBox(
              height: 50,
              child: ElevatedButton(
                  onPressed: () => _onlisten(),
                  child: Text(
                    'Capturar voz ',
                    style: TextStyle(fontSize: 30),
                  ),
                  style: ElevatedButton.styleFrom(primary: Colors.orange)),
            ),
          ]),
        ),
      ),
    );
  }
}
