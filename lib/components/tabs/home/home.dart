import 'package:flutter/material.dart';
import 'package:app_iris/resource/colors.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assets/images/Fondo1.jpg"),
              fit: BoxFit.cover)),
      child: Column(
        children: <Widget>[
          SizedBox(height: 20),
          Image.asset('assets/images/Jesi.png'),
          SizedBox(
            height: 20,
          ),
          Text(
            '¡Hola, Soy JESI!',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: kPrimaryColor,
              fontSize: 32,
              fontWeight: FontWeight.w900,
            ),
          ),
          SizedBox(height: 20),
          Text(
            'Soy tu asistente para la identificación de personas en tu vivienda. Selecciona una opción del menú inferior',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: kPrimaryColor,
              fontSize: 19,
              fontWeight: FontWeight.w300,
            ),
          ),
        ],
      ),
    );
  }
}
