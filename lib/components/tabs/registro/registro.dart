import 'package:app_iris/Screens/registro_exitoso.dart';
import 'package:app_iris/providers/usuario_provider.dart';
import 'package:flutter/material.dart';
import 'package:app_iris/resource/colors.dart';
import 'package:provider/provider.dart';

class Registro extends StatefulWidget {
  @override
  _Registro createState() => _Registro();
}

class _Registro extends State<Registro> {
  @override
  Widget build(BuildContext context) {
    final usuarioProvider = Provider.of<UsuarioProvider>(context);
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/Fondo2.jpg"),
                fit: BoxFit.cover)),
        child: Padding(
          padding: const EdgeInsets.all(45.0),
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Text('Por favor ingresa los datos y oprime REGISTRAR.',
                  style: TextStyle(fontSize: 17, color: darkColor)),
              SizedBox(
                height: 50,
              ),
              TextField(
                  decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      labelText: 'Nombres',
                      hintText: 'Nombres',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                      ),
                      prefixIcon: Icon(Icons.person)),
                  maxLength: 25,
                  onChanged: (value) {
                    usuarioProvider.changeNombre(value);
                  }),
              TextField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: 'Primer Apellido',
                    hintText: 'Primer Apellido',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.deepOrange,
                        width: 3.0,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    prefixIcon: Icon(Icons.person)),
                maxLength: 12,
                onChanged: (value) => usuarioProvider.changeprimapellido(value),
              ),
              TextField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: 'Segundo Apellido',
                    hintText: 'Segundo Apellido',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    prefixIcon: Icon(Icons.person)),
                maxLength: 12,
                onChanged: (value) => usuarioProvider.changesegapellido(value),
              ),
              TextField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: 'N° de identificación',
                    hintText: 'N° de identificación',
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.deepOrange,
                        width: 3.0,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    prefixIcon: Icon(Icons.person)),
                maxLength: 12,
                onChanged: (value) => usuarioProvider.changeCedula(value),
              ),
              TextField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: 'Dirección de residencia',
                    hintText: 'Dirección de residencia',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    prefixIcon: Icon(Icons.home)),
                maxLength: 40,
                onChanged: (value) => usuarioProvider.changedireccion(value),
              ),
              TextField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: 'Celular',
                    hintText: 'Celular',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    prefixIcon: Icon(Icons.phone)),
                keyboardType: TextInputType.number,
                maxLength: 10,
                onChanged: (value) => usuarioProvider.changecel(value),
              ),
              TextField(
                decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    labelText: 'RH',
                    hintText: 'RH',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                    ),
                    prefixIcon: Icon(Icons.local_hospital)),
                maxLength: 10,
                onChanged: (value) => usuarioProvider.changerh(value),
              ),
              SizedBox(
                height: 30,
              ),
              SizedBox(
                height: 120,
                child: Text(
                    'Autorizo de manera libre,previa inequivoca informada por JESI, mediante la imposición de mis datos a realizar el cotejo de mi información contra la base de datos de la Registraduría Nacional del Estado Civil (RNEC), con el fin de validar mi identidad.',
                    textAlign: TextAlign.justify,
                    style: TextStyle(fontSize: 15, color: darkColor)),
              ),
              SizedBox(
                height: 150,
                child: Text(
                    'Manifiesto que he sido informado que no estoy obligado a autorizar el tratamiento de dichos datos y que tengo derecho a conocer,actualizar y rectificar los datos personales proporcionados, a presentar quejas ante la Superintendecia de Industria y Comercio por el uso indebido de mis datos personales y a revocar esta autorizacion en los casos que sea procedente de acuerdo con la ley.',
                    textAlign: TextAlign.justify,
                    style: TextStyle(
                      fontSize: 15,
                      color: darkColor,
                    )),
              ),
              SizedBox(
                height: 30,
              ),
              ElevatedButton(
                  onPressed: () {
                    usuarioProvider.saveUsuario();
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => registroExitoso()));
                  },
                  child: Text(
                    'Registrar',
                    style: TextStyle(fontSize: 30),
                  ),
                  style: ElevatedButton.styleFrom(primary: secondaryColor)),
              SizedBox(
                height: 30,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
