import 'package:flutter/material.dart';
import 'package:app_iris/components/text_field_container.dart';
import 'package:app_iris/resource/colors.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final ValueChanged<String> onChanged;
  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon = Icons.person,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextField(
        //keyboardType: TextInputType.emailAddress,
        onChanged: onChanged,
        cursorColor: kPrimaryColor,
        decoration: InputDecoration(
          icon: Icon(icon, color: Colors.black54),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
