import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:app_iris/resource/usuario.dart';

class FirestoreService {
  FirebaseFirestore _db = FirebaseFirestore.instance;

  Future<void> saveUsuario(Usuario usuario) {
    return _db
        .collection('usuarios')
        .doc(usuario.usuarioID)
        .set(usuario.toMap());
  }
}
