import 'package:flutter_bloc/flutter_bloc.dart';

class BottomBarCubit extends Cubit<int> {
  BottomBarCubit() : super(0);

  void gethome() => emit(0);
  void getvoz() => emit(1);
  void getcamara() => emit(2);
  void getregistro() => emit(3);
}
