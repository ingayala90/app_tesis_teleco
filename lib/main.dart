import 'package:app_iris/providers/usuario_provider.dart';
import 'package:app_iris/resource/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:provider/provider.dart';
import './resource/theme.dart' as theme;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //await Firebase.initializeApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MyApp());
  });
  // SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
  //     .then((_) {
  //   runApp(preview.DevicePreview(
  //     enabled: true,
  //     builder: (context) => MyApp(),
  //   ));
  // });
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final _router = AppRouter();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (context) => UsuarioProvider(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          onGenerateRoute: _router.onGenerateRoute,
          title: 'Jesi',
          theme: theme.customTheme,
        ));
  }
}
