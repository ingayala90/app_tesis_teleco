import 'package:app_iris/Screens/Home_page.dart';
import 'package:app_iris/Screens/index_page.dart';
import 'package:app_iris/Screens/login_page.dart';
import 'package:app_iris/components/cubit/bottom_bar_cubit.dart';
import 'package:app_iris/components/tabs/home/home.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppRouter {
  Route onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/home':
        return MaterialPageRoute(
          builder: (_) => MultiBlocProvider(
            providers: [
              BlocProvider(create: (context) => BottomBarCubit()),
            ],
            child: HomePage(),
          ),
        );
      case '/':
        return MaterialPageRoute(builder: (_) => IndexPage());
      case '/login':
        return MaterialPageRoute(builder: (_) => Loginpage());
    }
  }
}
