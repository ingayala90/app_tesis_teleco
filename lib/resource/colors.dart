import 'dart:ui';
import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFf77600);
const kPrimaryLightColor = Color(0xFFE7F0FD);
const secondaryColor = Color(0xFF15A8F1);

const Color primaryColor = Color(0xFF2A639E);
const Color thirdColor = Color(0xFF181F2D);
const Color accentColor = Color(0xFF2A639E);
const Color lightColor = Color(0xFFFFFFFF);
const Color darkColor = Color(0xFF000000);

const Color verdeOscuro1 = Color(0xFF1e6e66);
const Color verdeBoton = Color(0xff005a44);
const Color celeste = Color(0xFF8bb9e2);
const Color grisFondo = Color(0xfff3f3f3);
const Color grisCard = Color(0xffffffff);
const Color grisOscuro = Color(0xff676767);
const Color fucsia = Color(0xffb93286);
const Color naranja = Color(0xfff5a11b);

const LinearGradient gradiente =
    LinearGradient(colors: [primaryColor, thirdColor]);
