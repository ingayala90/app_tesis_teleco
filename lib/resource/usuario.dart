class Usuario {
  final String usuarioID;
  final String nombre;
  final double cedula;
  final String primapellido;
  final String segapellido;
  final String direccion;
  final double cel;
  final String rh;

  Usuario(
      {this.usuarioID,
      this.nombre,
      this.cedula,
      this.primapellido,
      this.segapellido,
      this.direccion,
      this.cel,
      this.rh});

  Map<String, dynamic> toMap() {
    return {
      'usuarioID': usuarioID,
      'Nombres': nombre,
      'N° de identificación': cedula,
      'Primer Apellido': primapellido,
      'Segundo Apellido': segapellido,
      'Direccion de residencia': direccion,
      'Celualr': cel,
      'RH': rh,
    };
  }
}
