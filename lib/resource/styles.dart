import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:app_iris/resource/colors.dart' as colors;

// Style estandar para titulos con su alt.
final tituloStyle = TextStyle(fontSize: 16, color: colors.darkColor);
final tituloStyleAlt = tituloStyle.copyWith(color: colors.lightColor);

// Style de texto para NoContentText y su  alt en caso de que el fondo sea muy oscuro.
final noContentStyle = TextStyle(fontSize: 23, color: colors.darkColor);
final noContentStyleAlt = noContentStyle.copyWith(color: colors.lightColor);

// Style para el texto (no titulo) de NoNetPage y ConnectionRefussed.
final errorPageTextStyle = TextStyle(color: colors.lightColor, fontSize: 15.0);

final backgroundDecoration = BoxDecoration(
  color: colors.lightColor,
  image: DecorationImage(
    image: AssetImage('assets/fondos/fondo_verde.png'),
    alignment: Alignment.bottomCenter,
    fit: BoxFit.fitWidth,
  ),
);

final backgroundDecorationAlt = BoxDecoration(
  color: colors.lightColor,
  image: DecorationImage(
    image: AssetImage('assets/fondos/fondo_blanco.png'),
    alignment: Alignment.bottomCenter,
    fit: BoxFit.fitWidth,
  ),
);
