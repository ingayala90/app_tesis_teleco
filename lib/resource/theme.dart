import 'package:app_iris/resource/colors.dart' as colors;
import 'package:app_iris/resource/styles.dart' as styles;
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

// Custom theme = lightTheme.
final customTheme = ThemeData(
  brightness: Brightness.light,
  primaryColor: colors.primaryColor,
  //this is a secondary/accent color for widgets, like progress bar
  accentColor: colors.accentColor,
  backgroundColor: Colors.transparent,
  scaffoldBackgroundColor: Colors.transparent,
  canvasColor: colors.accentColor,
  shadowColor: colors.grisOscuro,
  textTheme: TextTheme(
    headline1: styles.tituloStyle,
    headline2: TextStyle(color: colors.secondaryColor),
    // Este es el texto que se usa en title como por ej AppBar.
    // headline6: TextStyle(
    //     fontSize: 36.0, fontStyle: FontStyle.italic, color: colors.lightColor),
    // Este es el texto que usa el Text() por defecto.
    bodyText1: TextStyle(fontSize: 14.0, color: colors.lightColor),
    bodyText2: TextStyle(fontSize: 14.0, color: colors.darkColor),
  ),
  appBarTheme: AppBarTheme(
    centerTitle: true,
    color: Colors.transparent,
    elevation: 0.0,
    brightness: Brightness.dark,
    iconTheme: IconThemeData(color: colors.darkColor),
    textTheme: TextTheme(
      // Custom theme para title de AppBar (por defecto)
      headline6: TextStyle(
        color: colors.darkColor,
        fontWeight: FontWeight.bold,
        fontSize: 20.0,
      ),
    ),
  ),
  bottomNavigationBarTheme: BottomNavigationBarThemeData(
    elevation: 0.0,
    backgroundColor: colors.primaryColor,
    selectedItemColor: colors.thirdColor,
    unselectedItemColor: colors.lightColor,
    type: BottomNavigationBarType.fixed,
    showUnselectedLabels: false,
    selectedLabelStyle: TextStyle(fontSize: 11),
  ),
  iconTheme: IconThemeData(
    color: colors.thirdColor,
  ),
);
