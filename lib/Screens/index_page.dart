import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'package:after_layout/after_layout.dart';

class IndexPage extends StatefulWidget {
  @override
  _IndexPageState createState() => _IndexPageState();
}

class _IndexPageState extends State<IndexPage>
    with AfterLayoutMixin<IndexPage> {
  int currentIndex = 0;
  List<Widget> widgets = [
    Material(
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/Universidad.png', height: 400),
          ],
        )),
    Material(
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/Carrera.png', height: 300),
          ],
        )),
    Material(
        color: Colors.transparent,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset('assets/images/logogrueso.png', height: 250),
          ],
        )),
  ];
  Timer timer;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(milliseconds: 1500), (timer) async {
      if (mounted) {
        setState(() {
          if (currentIndex + 1 == widgets.length) {
            currentIndex = 0;
          } else {
            currentIndex = currentIndex + 1;
          }
        });
      }
    });
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light
        .copyWith(statusBarColor: Colors.transparent));

    return Stack(
      children: [
        Container(
            decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/Fondo1.jpg'),
            fit: BoxFit.fill,
          ),
        )),
        SafeArea(
          top: true,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: AnimatedSwitcher(
                duration: Duration(milliseconds: 400),
                transitionBuilder: (Widget child, Animation<double> animation) {
                  return FadeTransition(child: child, opacity: animation);
                },
                child: Container(
                    child: Column(
                      children: [
                        Expanded(child: Container()),
                        widgets[currentIndex],
                        Expanded(child: Container()),
                      ],
                    ),
                    key: ValueKey<int>(currentIndex)),
              ),
            ),
          ),
        ),
      ],
    );
  }

  @override
  void afterFirstLayout(BuildContext context) async {
    Timer(Duration(milliseconds: 4000), () {
      Navigator.pushReplacementNamed(context, '/login');
    });
  }
}
