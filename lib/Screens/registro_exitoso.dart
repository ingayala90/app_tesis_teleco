import 'package:app_iris/Screens/Home_page.dart';
import 'package:app_iris/Screens/backbutton_widget.dart';
import 'package:app_iris/components/tabs/home/home.dart';
import 'package:app_iris/resource/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class registroExitoso extends StatefulWidget {
  @override
  _registroExitosoState createState() => _registroExitosoState();
}

class _registroExitosoState extends State<registroExitoso> {
  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Scaffold(
          body: Container(
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/Fondo1.jpg"),
                      fit: BoxFit.cover)),
              child: ListView(
                children: <Widget>[
                  SizedBox(
                    height: 250,
                  ),
                  Text(
                    '¡REGISTRO EXITOSO!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: kPrimaryColor,
                      fontSize: 40,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  SizedBox(height: 50),
                  Image.asset('assets/images/Exito.png', height: 300),
                  SizedBox(height: 50),
                  TextButton(
                    child: Text(
                      'Volver',
                      style: TextStyle(fontSize: 20),
                    ),
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, '/login'),
                  )
                ],
              ))),
      BackButtonWidget()
    ]);
  }
}
