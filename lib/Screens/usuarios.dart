import 'package:app_iris/components/tabs/registro/registro.dart';
import 'package:flutter/material.dart';

class usuarios extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Usuarios'),
        actions: <Widget>[
          IconButton(
              icon: Icon(
                Icons.add,
                size: 30.0,
              ),
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => Registro()));
              })
        ],
      ),
    );
  }
}
