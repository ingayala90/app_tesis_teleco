import 'package:app_iris/components/rounded_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app_iris/components/rounded_input_field.dart';

class Resetscreen extends StatefulWidget {
  @override
  _Resetscreen createState() => _Resetscreen();
}

class _Resetscreen extends State<Resetscreen> {
  String _email;
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/Fondo1.jpg"),
          fit: BoxFit.fill,
        )),
        child: Padding(
          padding: EdgeInsets.all(70),
          child: Column(
            children: <Widget>[
              SizedBox(height: 200),
              RoundedInputField(
                hintText: "Usuario",
                onChanged: (value) {
                  setState(() {
                    _email = value.trim();
                  });
                },
              ),
              SizedBox(height: 50),
              RoundedButton(
                  text: "RESET",
                  press: () {
                    auth.sendPasswordResetEmail(email: _email);
                    Navigator.of(context).pop();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
