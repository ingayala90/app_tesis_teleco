import 'package:flutter/material.dart';
import 'package:app_iris/resource/colors.dart' as colors;

class BackButtonWidget extends StatelessWidget {
  final Function onTap;
  BackButtonWidget({this.onTap});
  @override
  Widget build(BuildContext context) {
    return Positioned(
        top: 10,
        left: 15,
        child: GestureDetector(
          onTap: this.onTap == null
              ? () {
                  Navigator.pop(context);
                }
              : onTap,
          child: Container(
              decoration: BoxDecoration(
                  color: colors.grisCard,
                  borderRadius: BorderRadius.circular(5)),
              child: Icon(
                Icons.arrow_back,
                color: colors.verdeBoton,
                size: 30,
              )),
        ));
  }
}
