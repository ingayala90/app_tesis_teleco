import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Home_page.dart';

class Verifyscreen extends StatefulWidget {
  @override
  _VerifyscreenState createState() => _VerifyscreenState();
}

class _VerifyscreenState extends State<Verifyscreen> {
  final auth = FirebaseAuth.instance;
  User user;
  Timer timer;

  @override
  void initState() {
    user = auth.currentUser;
    user.sendEmailVerification();

    Timer.periodic(Duration(seconds: 5), (timer) {
      checkEmailVerified();
    });
    super.initState();
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/Fondo2.jpg"),
                    fit: BoxFit.fill)),
            child: Padding(
                padding: EdgeInsets.all(70.0),
                child: Column(children: <Widget>[
                  SizedBox(height: 100),
                  SizedBox(
                    height: 100,
                    child: Text(
                      'Un email fue enviado a ${user.email} por favor verifque',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 30,
                      ),
                    ),
                  ),
                  SizedBox(height: 40),
                  Image.asset('assets/images/Jesi.png', height: 400),
                  SizedBox(height: 40),
                  TextButton(
                    child: Text('Volver'),
                    onPressed: () =>
                        Navigator.pushReplacementNamed(context, '/login'),
                  ),
                ]))));
  }

  Future<void> checkEmailVerified() async {
    user = auth.currentUser;
    await user.reload();
    if (user.emailVerified) {
      timer.cancel();
      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
    }
  }
}
