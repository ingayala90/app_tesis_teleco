import 'package:app_iris/Screens/login_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app_iris/resource/colors.dart' as colors;

class CustomDrawer extends StatelessWidget {
  static final CustomDrawer _instance = CustomDrawer();
  static CustomDrawer get instance => _instance;
  final auth = FirebaseAuth.instance;
  @override
  Widget build(BuildContext context) {
    Radius bordes = Radius.circular(20);
    double drawerWidth = MediaQuery.of(context).size.width * 0.70;
    double drawerHeight = MediaQuery.of(context).size.height * 0.94;
    return Container(
      width: drawerWidth,
      height: drawerHeight,
      decoration: BoxDecoration(
        color: colors.lightColor,
        borderRadius: BorderRadius.only(topRight: bordes, bottomRight: bordes),
      ),
      child: Column(
        children: [
          _logoBlanco(context),
          Expanded(
            child: ListView(
              children: [
                _drawerDivider(),
                ListTile(
                  title: Text(
                    'Notificaciones',
                    style: TextStyle(color: colors.kPrimaryColor),
                  ),
                  leading: Icon(
                    Icons.notification_important,
                    color: colors.kPrimaryColor,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                _drawerDivider(),
                SizedBox(height: 40),
              ],
            ),
          ),
          _cerrarSesion(context),
          SizedBox(height: 20.0)
        ],
      ),
    );
  }

  _logoBlanco(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(50),
      height: MediaQuery.of(context).size.height * 0.20,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/images/logo.png',
          ),
          fit: BoxFit.scaleDown,
        ),
      ),
    );
  }

  _drawerButton(BuildContext context, String text, Function() onTap) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: Container(
          height: 25,
          margin: EdgeInsets.only(top: 10, bottom: 5, left: 25, right: 20),
          child: Text(
            '   ' + text[0].toUpperCase() + text.substring(1).toLowerCase(),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
      ),
    );
  }

  _drawerDivider() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 25),
      height: 0.5,
      color: Colors.grey,
    );
  }

  _cerrarSesion(context) {
    return InkWell(
      onTap: () async {
        auth.signOut();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => Loginpage()));
      },
      child: Container(
        decoration: BoxDecoration(
          color: colors.secondaryColor,
          borderRadius: BorderRadius.circular(10),
        ),
        margin: EdgeInsets.only(left: 30, right: 30, top: 10),
        height: 40,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.power_settings_new,
              color: colors.lightColor,
            ),
            SizedBox(width: 10),
            Text('Cerrar Sesión', style: TextStyle(color: colors.lightColor))
          ],
        ),
      ),
    );
  }
}
