import 'package:app_iris/Screens/custom_drawer.dart';
import 'package:app_iris/Screens/login_page.dart';
import 'package:app_iris/components/cubit/bottom_bar_cubit.dart';
import 'package:app_iris/components/rounded_button.dart';
import 'package:app_iris/components/tabs/camara/Camara.dart';
import 'package:app_iris/components/tabs/home/home.dart';
import 'package:app_iris/components/tabs/registro/registro.dart';
import 'package:app_iris/components/tabs/voz/voz.dart';
import 'package:flutter/material.dart';
import 'package:app_iris/resource/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePage> {
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    String mail = auth.currentUser.email;
    // String usuario = auth.currentUser.displayName;
    return Scaffold(
        //backgroundColor: Colors.transparent,
        //drawerScrimColor: Colors.transparent,

        appBar: PreferredSize(
          preferredSize: AppBar(
            shadowColor: Colors.orange,
          ).preferredSize,
          child: BlocBuilder<BottomBarCubit, int>(builder: (context, state) {
            return AppBar(
              title: _getTitle(state),
            );
          }),
        ),
        drawer: CustomDrawer(),
        body: Container(
            child: BlocBuilder<BottomBarCubit, int>(builder: (context, state) {
          return _buildBody(state);
        })),
        bottomNavigationBar: _bottombar(context));
  }

  Widget _bottombar(BuildContext context) {
    return BlocBuilder<BottomBarCubit, int>(
      builder: (context, state) {
        return BottomNavigationBar(
          backgroundColor: secondaryColor,
          currentIndex: state,
          onTap: (index) => _getChangeBottomNav(context, index),
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              label: 'Home',
              backgroundColor: kPrimaryColor,
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.mic,
              ),
              label: 'Microfono',
              backgroundColor: kPrimaryColor,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.camera),
              label: 'Camara',
              backgroundColor: kPrimaryColor,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.note),
              label: 'Registro',
              backgroundColor: kPrimaryColor,
            ),
          ],
        );
      },
    );
  }

  void _getChangeBottomNav(BuildContext context, int index) {
    switch (index) {
      case 0:
        context.bloc<BottomBarCubit>().gethome();
        break;
      case 1:
        context.bloc<BottomBarCubit>().getvoz();
        break;
      case 2:
        context.bloc<BottomBarCubit>().getcamara();
        break;
      case 3:
        context.bloc<BottomBarCubit>().getregistro();
        break;
      default:
    }
  }

  Widget _getTitle(int index) {
    switch (index) {
      case 0:
        return Text(
          'JESI',
          style: TextStyle(fontWeight: FontWeight.bold),
        );
        break;
      case 1:
        return Text(
          'VOZ',
          style: TextStyle(fontWeight: FontWeight.bold),
        );
        break;
      case 2:
        return Text(
          'CAMARA',
          style: TextStyle(fontWeight: FontWeight.bold),
        );
        break;
      case 3:
        return Text(
          'REGISTRO',
          style: TextStyle(fontWeight: FontWeight.bold),
        );
        break;
      default:
        return Text(
          'JESI',
          style: TextStyle(fontWeight: FontWeight.bold),
        );
        break;
    }
  }

  Widget _buildBody(int index) {
    switch (index) {
      case 0:
        return Home();
        break;
      case 1:
        return Voz();
        break;
      case 2:
        return Camara();
        break;
      case 3:
        return Registro();
        break;
      default:
        return Home();
    }
  }
}
