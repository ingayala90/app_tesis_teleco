import 'package:app_iris/Screens/reset.dart';
import 'package:app_iris/Screens/verify.dart';
import 'package:app_iris/components/rounded_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app_iris/Screens/Home_page.dart';
import 'package:app_iris/components/already_have_an_account_acheck.dart';
import 'package:app_iris/components/rounded_input_field.dart';
import 'package:app_iris/components/rounded_password_field.dart';
import 'package:fluttertoast/fluttertoast.dart';

class Loginpage extends StatefulWidget {
  @override
  _Loginpage createState() => _Loginpage();
}

class _Loginpage extends State<Loginpage> {
  String _email, _password;
  final auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          image: AssetImage("assets/images/Fondo.jpg"),
          fit: BoxFit.fill,
        )),
        child: Column(
          children: <Widget>[
            SizedBox(height: 150),
            Image.asset('assets/images/logogrueso.png', height: 200),
            SizedBox(height: 150),
            RoundedInputField(
              hintText: "Usuario",
              onChanged: (value) {
                setState(() {
                  _email = value.trim();
                });
              },
            ),
            RoundedPasswordField(
              onChanged: (value) {
                setState(() {
                  _password = value.trim();
                });
              },
            ),
            SizedBox(height: 0),
            RoundedButton(
                text: "INICIAR SESIÓN",
                press: () => _signin(_email, _password)),
            SizedBox(height: 20),
            AlreadyHaveAnAccountCheck(press: () => _signup(_email, _password)),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextButton(
                  child: Text('Olvidó su contraseña?'),
                  onPressed: () => Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => Resetscreen())),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  _signin(String _email, String _password) async {
    try {
      await auth.signInWithEmailAndPassword(email: _email, password: _password);

      //success
      Navigator.pushReplacementNamed(context, '/home');
    } on FirebaseAuthException catch (error) {
      Fluttertoast.showToast(
        msg: error.message,
        backgroundColor: Colors.red,
        fontSize: 5,
        gravity: ToastGravity.BOTTOM_LEFT,
        toastLength: Toast.LENGTH_LONG,
      );
    }
  }

  _signup(String _email, String _password) async {
    try {
      await auth.createUserWithEmailAndPassword(
          email: _email, password: _password);

      //success
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => Verifyscreen()));
    } on FirebaseAuthException catch (error) {
      Fluttertoast.showToast(
        msg: error.message,
        fontSize: 5,
        backgroundColor: Colors.red,
        gravity: ToastGravity.BOTTOM_LEFT,
        toastLength: Toast.LENGTH_LONG,
      );
    }
  }
}
